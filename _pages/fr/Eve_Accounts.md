---
layout: single
permalink: /fr/Eve_Accounts/
title: Eve Pentest - Comptes en ligne
header:
  overlay_color: "#000"
  overlay_filter: "0.3"
  overlay_image: /assets/images/guide/saintandrewcut.jpg
  actions:
    - label: "Lire le guide complet"
      url: /Eve_Pentest/
excerpt: "Eve Pentest est une dominatrice de sécurité numérique qui sait comment sécuriser ses limites et créer un espace safe pour jouer. Eve en connaît beaucoup sur la technologie et comment Internet fonctionne. Elle utilise beaucoup de services en ligne dans son travail et sait comment se protéger contre de potentiels harceleurs, hackers ou haters. Elle travaille dans un donjon qui lui est dédié et garde sa vie personnelle complètement séparée de son travail."
---

# Les comptes en ligne de Eve

Pour faire de la publicité à ses comptes en ligne, Eve utilise à la fois les réseaux sociaux mainstream et des plateformes spécialisées dans le BDSM et le travail du sexe. Dans le même temps, elle utilises des comptes différents sur quasiment tous ces sites pour communiquer avec sa famille, ses ami-e-s et amant-e-s, et bien entendu, elle utilise un compte différent pour chacun-e d'entre elleux.


![](../assets/images/guide/saintandrew.jpg){:height="300px" width="300px"}

Elle possède les comptes suivants:

| Platforme  | Contact | Vrai ou faux nom | Niveau de confiance | Travail? | Compte jetable |
|------------|---------|----------------- |---------------------|-------|----------------|
| Facebook   | Ami-e-s    |  Faux nom réaliste 1 |  Haut     |  Oui | Oui             |
| Facebook   | Famille   |  Vrai Nom        |  Moyen        |  Non  |    Non              |
| Instagram  | Ami-e-s  |  Faux nom réaliste 2 |  Haut     |  Oui |  Oui           |
| Instagram  | Travail     |  Nom de travail  |  Bas    | Bien sur ! | Oui             |
| Twitter    | Public   |  Nom de travail |  Bas           | Oui | Non              |
| Fetlife    | Ami-e-s et amant-e-s | Faux nom 2 |  Haut      | Bien sur ! | Non   |
| Fetlife    | Travail     |  Nom de travail |  Bas     |  Bien sur ! | Oui      |
| onlyfans <br /> niteflirt <br /> eros | Travail | Nom de travail | Bas | Bien Sur! | Non |
| Switter.at | Public  | Nom de travail |  Haut   | Oui | Non   |

Eve considère comme jetable tous les comptes sur des services qui demandent le nom officiel, ou limitent le contenu pour adulte ou le travial du sexe (voir [les conditions d'utilisation](#terms-of-use) ci-dessous). Elle essaie d'utiliser un faux nom réaliste pour Facebook pour éviter d'être détectée par les robots de Facebook qui identifient les faux noms, mais elle sait que son compte peut-être suspendu à tout moment parce que Facebook lui demanderait une pièce d'identité.

Pour garder ses comptes vraiment séparés, elle évite de les connecter à sa vrai identité si elle n'a pas décidé d'utiliser sa vrai identité lors de la création (comme elle le fait pour ses comptes pour discuter avec sa famille, ou sa banque). Par ailleurs, elle garde des comptes plus stables où elle peut toujours être trouvée sur des plateformes dédiées comme [Switter](https://switter.at/about) (un réseau social accueillant pour les travailleur-euse-s du sexe) ou sur des plateformes commerciales de travail du sexe, et dit à ses contacts qu'iels peuvent la trouver sur ces comptes si ses autres comptes sont supprimés ou bloqués soudainement.

Néanmoins, Eve ne prends pas pour acquis que ces comptes resteront éternellement et garde une sauvegarde de leur contenu sur son ordinateur mais aussi sur un disque dur externe.

### Comptes séparés

Pour garder ses comptes séparés et être sure qu'ils ne sont pas connectés les uns aux autres, Eve suit les règles suivantes :

* Elle ne gère ses comptes de travail que depuis ses appareils de travail.
* Elle ne suit pas les mêmes personnes avec des comptes différents reliés à des identités différentes.
* Elle fait attention de ne pas suivre ses comptes entre eux, et de ne jamais partager le même contenu avec des identités différentes.
* Even sait que la plupart des plateformes de réseaux sociaux vont afficher sa localisation dès que possible, donc elle désactive la géolocalisation sur ses applications de téléphone, et n'active le GPS que quand elle en a vraiment besoin.
* Eve sait également que beaucoup d'applications et appareils photos vont inclure des méta-données dans ses photos, qui peuvent inclure la date, here et localisaton de la photo entre autres choses. Ces méta-données seront inclues dans les photos et vidéos qu'elle partage en ligne alors elle vérifie toujours que la géolocalisation est désactivée quand elle prend des photos ou tourne des vidéos.
